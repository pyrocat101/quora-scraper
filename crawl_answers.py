#!/usr/bin/env python

import json
import multiprocessing
import os
import re
import subprocess
import sys

JOB = os.environ.get("JOB") or 1
# most file system limit max length of file name to 255
NAME_MAX = 255

def usage():
    print "Usage: python crawler.py <topic-txt>"
    sys.exit(0)

def spawn(*args):
    p = subprocess.Popen(args, stdout=sys.stdout, stderr=sys.stderr)
    return p.wait()

def get_topic_name(url):
    topic_name = re.compile(r"quora\.com\/([^\/]+)(?:\/all_questions)?\/?$")
    return re.search(topic_name, url).groups()[0]

def fetch_questions(url):
    topic_name = get_topic_name(url)
    ret = spawn("casperjs", "get_questions.coffee", url)
    if ret != 0: raise RuntimeError("Fetching question list failed.")
    urls = []
    with open(os.path.join('data', topic_name + '.json')) as f:
        data = json.load(f)
        questions = data['questions']
        name = data['name']
        for entry in questions:
            urls.append(entry['link'])
    return name, urls

def scrape_it(url):
    return spawn("casperjs", "../../get_answers.coffee", url)

def fetch_answers(topic_name, questions):
    pwd = os.getcwd()
    data_dir = os.path.join('data', topic_name)
    if not os.path.isdir(data_dir): os.mkdir(data_dir)
    # crawl every question page
    os.chdir(data_dir)
    pool = multiprocessing.Pool(int(JOB))
    links = map(lambda x: "http://www.quora.com" + x, questions)
    pool.map(scrape_it, links)
    os.chdir(pwd)

def get_topic_urls():
    with open(sys.argv[1]) as f:
        return set(f.read().split())

if __name__ == '__main__':
    if len(sys.argv) != 2: usage()
    topic_urls = get_topic_urls()
    for url in topic_urls:
        topic_name, questions = fetch_questions(url)
        fetch_answers(topic_name, questions)
