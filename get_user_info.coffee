system = require 'system'
casper = require('casper').create
  verbose: true
  logLevel: (if system.env['DEBUG'] then 'debug' else 'warning')
  waitTimeout: 60000
  pageSettings:
    loadImages: false
    loadPlugins: false
    # userAgent: 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.2 Safari/537.36'
    userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36'
colorizer = require('colorizer').create('Colorizer')

URL = ''
NAME  = ''
INFO = {}

if casper.cli.args.length is 0
  casper.die 'Please provide a Quora user URL'
else
  URL = casper.cli.get(0)
  NAME = URL.match(/quora\.com\/(.+)(?:\/)?$/)[1]

casper.start "#{URL}?share=1", ->
  getUserStat()

casper.thenOpen "#{URL}/followers?share=1", ->
  if casper.exists '.pager_next.action_button'
    tryLoadMore()

casper.then ->
  getFollowers()

casper.thenOpen "#{URL}/following?share=1", ->
  if casper.exists '.pager_next.action_button'
    tryLoadMore()

casper.then ->
  getFollowing()

casper.then ->
  output = "users/#{NAME}.json"
  require('fs').write output, JSON.stringify(INFO), 'w'

casper.then ->
  @echo "#{colorizer.colorize('SCRAPED', 'INFO')} #{URL}"

getUserStat = ->
  labels = casper.getElementsInfo('.link_label').slice(1)
  stats = {}
  labels.forEach (l) ->
    l = l.text.trim().split(' ')
    attr = l[0]
    count = l[1]
    stats[attr] = count
  INFO.url       = URL
  INFO.user_id   = NAME
  INFO.topics    = stats.Topics or 0
  INFO.blogs     = stats.Blogs or 0
  INFO.questions = stats.Questions or 0
  INFO.answers   = stats.Answers or 0
  INFO.edits     = stats.Edits or 0

getFollowers = ->
  INFO.followers = scrapeIt()

getFollowing = ->
  INFO.following = scrapeIt()

scrapeIt = ->
  if casper.exists '.name a'
    users = casper.getElementsInfo '.name a'
    users.map (u) ->
      link: u.attributes.href
      name: u.text
  else
    []

tryLoadMore = ->
  casper.click '.pager_next.action_button'
  if casper.getElementInfo('.pager_next[id$=loading]').visible
    casper.waitWhileVisible '.pager_next[id$=loading]', ->
      tryLoadMore()

casper.run()