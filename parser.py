import re
from itertools import imap, izip

answer_fields = ['answer_id',
                 'question_id',
                 'user_id',
                 'date',
                 'number_of_upvotes',
                 'users_who_upvoted',
                 'topics',
                 'current_topic',
                 'question_text',
                 'answer_text']
user_fields = ['user_id',
               'number_of_topics',
               'number_of_blogs',
               'number_of_questions',
               'number_of_answers',
               'number_of_edits',
               'followers',
               'following']
quoted = re.compile('{{{(.+)}}}')

def unquote(item):
    m = quoted.match(item)
    return m.groups()[0] if m else item

def parse_line(line, fields):
    values = imap(str.strip, imap(unquote, line.split(', ')))
    return dict(izip(fields, values))

def parse_user(line):
    return parse_line(line, user_fields)

def parse_answer(line):
    return parse_line(line, answer_fields)

if __name__ == '__main__':
    with open('out/users.csv') as f:
        parse_user(f.readline())
    with open('out/answers.csv') as f:
        parse_answer(f.readline())