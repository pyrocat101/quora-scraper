system = require 'system'
casper = require('casper').create
  verbose: true
  logLevel: (if system.env['DEBUG'] then 'debug' else 'warning')
  waitTimeout: 60000
  pageSettings:
    loadImages: false
    loadPlugins: false
    userAgent: 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.2 Safari/537.36'

URL = ''
NAME  = ''

if casper.cli.args.length is 0
  casper.die 'Please provide a Quora topic URL'
else
  URL = casper.cli.get(0)
  unless /\/all_questions(\/)?$/.test(URL)
    URL += '/all_questions'
  NAME = URL.match(/quora\.com\/(.+)\/all_questions(?:\/)?/)[1] + '.json'
  URL += '?share=1'

casper.start URL, ->
  tryLoadMoreQuestions()

casper.then ->
  name = getTopicName()
  questions = getAllQuestions()
  data =
    name: name
    questions: questions
  # write to file
  require('fs').write "data/#{NAME}", JSON.stringify(data), 'w'

casper.then ->
  @echo "SCRAPED #{URL}", 'GREEN_BAR'

tryLoadMoreQuestions = ->
  casper.click '.pager_next.action_button'
  if casper.getElementInfo('.pager_next[id$=loading]').visible
    casper.waitWhileVisible '.pager_next[id$=loading]', ->
      tryLoadMoreQuestions()

getAllQuestions = ->
  # remove clutters
  casper.evaluate ->
    window.$('a.question_link .question_text_icons').remove()
  casper.getElementsInfo('a.question_link').map (e) ->
    question = e.text.trim()
    link = e.attributes.href
    { question: question, link: link }

getTopicName = ->
  casper.getTitle().match(/^All Questions on (.+) - Quora$/)[1]

casper.run()