system = require 'system'
casper = require('casper').create
  verbose: true
  logLevel: (if system.env['DEBUG'] then 'debug' else 'warning')
  waitTimeout: 60000
  pageSettings:
    loadImages: false
    loadPlugins: false
    userAgent: 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.2 Safari/537.36'
colorizer = require('colorizer').create('Colorizer')

# Most file systems limit the max file name length to 255
MAX_NAME = 255

URL = ''
ANS_COUNT = -1
ANS_LOADED = -1

if casper.cli.args.length is 0
  casper.die 'Please provide a Quora URL'
else
  URL = casper.cli.get(0)
  unless /\?share=1$/.test(URL)
    URL += '?share=1'

casper.start URL, ->
  ANS_COUNT = parseInt @fetchText('.answer_header_text>h3')
  if isNaN(ANS_COUNT) then ANS_COUNT = 0
  # report total answer count
  # @echo "Navigating to #{URL}"
  # @echo "Scraping #{ANS_COUNT} answers"
  # if more answers can be loaded, try to load them all
  ANS_LOADED = countLoadedAnswers()
  tryLoadMoreAnswers()

casper.then ->
  # if `more` links exists, try expand them
  tryExpandAnswers()

casper.then ->
  tryExpandVotersAndWait()

casper.then ->
  # scrape all answers
  @log 'Scraping data...', 'debug'
  data = scrapeThemAll()
  # write to file
  @log 'Writing to JSON file...', 'debug'
  # file name
  output = getJSONFilename(data.url)
  require('fs').write output, JSON.stringify(data), 'w'

casper.then ->
  @echo "#{colorizer.colorize('SCRAPED', 'INFO')} #{URL}"

countLoadedAnswers = ->
  casper.getElementsInfo('.pagedlist_item').length

tryLoadMoreAnswers = ->
  # wait for more answers to be loaded
  moreAnswers = ->
    latestCount = countLoadedAnswers()
    if latestCount > ANS_LOADED
      # more answers are loaded
      ANS_LOADED = latestCount
      true
    else
      false
  onLoadSuccess = ->
    @log 'More answers loaded', 'debug'
    # expand voters now for speed up
    tryExpandAnswers()
    @emit 'answers.loaded'
  onLoadTimeout = ->
    @die 'Loading more answers timeout'
  # if all answers are loaded, just return
  return if ANS_LOADED >= ANS_COUNT
  casper.log 'Loading more answers...', 'debug'
  # click to load more answers
  casper.click '.pager_next.action_button'
  casper.waitFor moreAnswers, onLoadSuccess, onLoadTimeout

casper.on 'answers.loaded', tryLoadMoreAnswers

tryExpandAnswers = ->
  moreLink = 'a[id$=switch_link]'
  return unless casper.exists moreLink
  casper.log 'Expanding answers content...', 'debug'
  casper.getElementsAttribute(moreLink, 'id').forEach (id) ->
    id = "##{id}"
    casper.mouseEvent 'mouseover', id
    casper.click id
  casper.waitWhileVisible "#{moreLink} + span", (->), ->
    @log 'Expand answers timeout', 'error'
  # WTF?
  casper.getElementsAttribute('[id$=_truncated]', 'id').forEach (id) ->
    id = "##{id}"
    casper.mouseEvent 'mouseover', id

tryExpandVoters = ->
  moreVoterLink = '.answer_voters [id$=toggle_link]'
  if casper.exists moreVoterLink
    casper.getElementsAttribute(moreVoterLink, 'id').forEach (id) ->
      id = "##{id}"
      casper.mouseEvent 'mouseover', id
      casper.click id
  # wait until loading text disappear
  # TODO: waiting for new answers and expanding voters simultaneously.
  # casper.waitWhileVisible "#{moreVoterLink} + span"

tryExpandVotersAndWait = ->
  tryExpandVoters()
  onExpandTimeout = -> @log 'Expand voters timeout', 'error'
  casper.waitWhileVisible '.answer_voters [id$=toggle_link] + span', (->), onExpandTimeout, 60000

scrapeThemAll = ->
  # Get question and detail
  question = getQuestion()
  topics = getTopics()
  questionDetail = getQuestionDetail()
  questionUrl = casper.getCurrentUrl()
  # Handle every answer
  answers = casper.getElementsAttribute('.row[id$=_answer]', 'id').map (id) ->
    # id of an enclosing element
    id = "##{id}"
    # get author name and link
    author = getAuthorInfo id
    # get answer content
    content = getAnswerContent id
    # get answer vote
    vote = getAnswerVote id
    voters = getAnswerVoters id
    # get answer date
    date = getAnswerDate id
    # compose an answer entry
    { author: author, content: content, vote: vote, voters: voters, date: date }
  return {
    question: question
    detail:   questionDetail
    url:      questionUrl
    answers:  answers
    topics:   topics
  }

getQuestion = ->
  casper.getTitle().replace /\s-\sQuora$/, ''

getQuestionDetail = ->
  casper.fetchText '.question_details_text'

getAnswerVote = (id) ->
  # Caveat: this is not accurate number (e.g. 3.1k)
  casper.fetchText "#{id} .rating_value .numbers"

getAnswerVoters = (id) ->
  voters = "#{id} .answer_voters"
  # No voters :-(
  return [] unless casper.exists voters
  # Deal with Anonymous
  anonymous = casper.fetchText(voters).match(/\bAnonymous\b/g)
  if anonymous isnt null
    anonymous = anonymous.map (a) -> { name: "Anonymous", link: null }
  else
    anonymous = []
  # User voter
  if casper.exists "#{voters} .user"
    voteUsers = casper.getElementsInfo("#{voters} .user").map (e) ->
      { name: e.text.trim(), link: e.attributes.href }
  else
    voteUsers = []
  # Voter = user voter + anonymous voter
  voteUsers.concat anonymous

getAuthorInfo = (id) ->
  # Non-anonymous
  if casper.exists "#{id} .answer_user_wrapper a.user"
    e = casper.getElementInfo "#{id} .answer_user_wrapper a.user"
    { name: e.text.trim(), link: e.attributes.href }
  # Anonymous user
  else
    { name: "Anonymous", link: null }

getAnswerContent = (id) ->
  if casper.exists "#{id} [id$=answer_content] [id$=container]"
    e = casper.getElementInfo "#{id} [id$=answer_content] [id$=container]"
    e.text
  else if casper.exists "#{id} .answer_flag_note_text"
    # answer with `answer_flag_note_text`
    casper.fetchText "#{id} .answer_content > div + div"
  else
    # unknown answer content
    casper.log "Cannot scrape answer content in #{id}", 'error'
    ""

getAnswerDate = (id) ->
  casper.getElementInfo("#{id} .datetime").text.trim()

getJSONFilename = (url) ->
  url = url.replace /\?share=1/, ''
  url = url.replace /\//, ''
  idx = findRight url, '/'
  name = decodeURIComponent url.slice(idx + 1)
  # truncate long file name
  name = name.slice(0, MAX_NAME - 5)
  "#{name}.json"

findRight = (s, c) ->
  s = s.toString()
  for i in [(s.length - 1)..0]
    if s[i] is c
      break
  return i

getTopics = ->
  topics = casper.getElementsInfo('.question_topics .name_text')
  topics.map (t) -> t.text.trim()

# kick off
casper.run()
