#!/usr/bin/env python

import os
import sys
import json
import glob
import codecs
import urlparse
import multiprocessing
import subprocess
from itertools import chain, imap

reload(sys)
sys.setdefaultencoding('utf-8')

JOB = os.environ.get("JOB") or 1

def is_anonymous(u):
    return u['link'] is None or u['name'] is 'Anonymous'

def get_user_id(u):
    return urlparse.urljoin('http://www.quora.com/', u['link'])

def get_all_users():
    files = glob.glob('data/*/*.json')
    users = chain.from_iterable(imap(read_users, files))
    return set(users)

def read_users(filename):
    users = []
    with codecs.open(filename, encoding='utf-8') as f:
        data = json.load(f)
        answers = data['answers']
        for ans in answers:
            author = ans['author']
            if not is_anonymous(author):
                # add author
                users.append(get_user_id(author))
            # add voters
            for voter in ans['voters']:
                if not is_anonymous(voter):
                    users.append(get_user_id(voter))
        return users

def fetch_users(users):
    pool = multiprocessing.Pool(int(JOB))
    pool.map(scrape_it, users)

def scrape_it(url):
    return spawn("casperjs", "get_user_info.coffee", url)

def spawn(*args):
    p = subprocess.Popen(args, stdout=sys.stdout, stderr=sys.stderr)
    return p.wait()

def get_existing_users():
    files = glob.glob('users/*.json')
    users = []
    for f in files:
        with codecs.open(f, encoding='utf-8') as f:
            data = json.load(f)
            users.append(data['url'])
    return set(users)

if __name__ == '__main__':
    users = get_all_users()
    existing_users = get_existing_users()
    users = users - existing_users
    with open('user_list') as f:
        users = set(f.read().split())
    fetch_users(users)
