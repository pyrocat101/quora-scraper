#!/usr/bin/env python

import codecs
import sys
import json
import glob
import urlparse
import os
from collections import namedtuple
from itertools import ifilterfalse, chain, imap

reload(sys)
sys.setdefaultencoding('utf-8')

Answer = namedtuple('Answer',
                    ['answer_id',
                     'question_id',
                     'user_id',
                     'date',
                     'number_of_upvotes',
                     'users_who_upvoted',
                     'topics',
                     'current_topic',
                     'question_text',
                     'answer_text'])

def get_user_id(u):
    return urlparse.urljoin('http://www.quora.com/', u['link'])

def is_anonymous(u):
    return u['link'] is None or u['name'] is 'Anonymous'

def get_topic_dirs():
    return glob.glob('data/*/')

def get_all_json(root):
    return glob.glob(os.path.join(root, '*.json'))

def process_date(d):
    # TODO: add year
    return d.replace(',', '')

def process_answers(data, current_topic):
    question_id = data['url'].replace('?share=1', '')
    topics = data['topics']
    question_text = data['detail']#.replace(u'\xa0', u' ')
    question_text = question_text.encode('utf-8')
    answers = data['answers']
    res = []

    def get_author_id(u):
        if is_anonymous(u):
            user_id = 'Anonymous-%d' % get_author_id.counter
            get_author_id.counter += 1
            return user_id
        else:
            return get_user_id(u)
    get_author_id.counter = 1

    for ans in answers:
        user_id = get_author_id(ans['author'])
        answer_id = '%s-%s' % (question_id, user_id)
        date = process_date(ans['date'])
        number_of_upvotes = ans['vote']
        users_who_upvoted = map(get_user_id,
                                ifilterfalse(is_anonymous, ans['voters']))
        answer_text = ans['content']
        if isinstance(answer_text, unicode):
            # answer_text = answer_text.replace(u'\xa0', u' ')
            answer_text = answer_text.encode('utf-8')
        #     # answer_text = answer_text.strip()
        #     answer_text = answer_text.replace(u'\xa0', u' ').strip()
        if answer_text is None:
            raise
            # print 'Fuck!'
        entry = Answer(answer_id,
                       question_id,
                       user_id,
                       date,
                       number_of_upvotes,
                       users_who_upvoted,
                       topics,
                       current_topic,
                       question_text,
                       answer_text)
        res.append(entry)
    return res

def process_json(f, current_topic):
    with codecs.open(f, encoding="utf-8") as fp:
        data = json.load(fp)
        return process_answers(data, current_topic)

def answer_to_string(ans):
    return '%s, %s, %s, %s, %s, {{{%s}}}, {{{%s}}}, %s, {{{%s}}}, {{{%s}}}' % \
           (ans.answer_id, ans.question_id, ans.user_id, ans.date,
            ans.number_of_upvotes, ', '.join(ans.users_who_upvoted),
            ', '.join(ans.topics), ans.current_topic,
            ans.question_text, ans.answer_text)

def write_answers(answers, csv_filename):
    with open(csv_filename, 'w') as f:
        for ans in answers:
            f.write(answer_to_string(ans))
            f.write('\n')

def main():
    all_answers = []
    for d in get_topic_dirs():
        topic_name = os.path.basename(d[:-1])
        files = get_all_json(d)
        answers = imap(lambda f: process_json(f, topic_name), files)
        answers = chain.from_iterable(answers)
        all_answers.extend(answers)
    # write to csv
    write_answers(all_answers, 'out/answers.csv')

if __name__ == '__main__':
    main()