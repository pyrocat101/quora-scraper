#!/usr/bin/env python

import os
import sys
import json
import glob
import codecs
import urlparse
from collections import namedtuple
from itertools import chain, imap

reload(sys)
sys.setdefaultencoding('utf-8')

User = namedtuple('User',
                  ['user_id',
                   'number_of_topics',
                   'number_of_blogs',
                   'number_of_questions',
                   'number_of_answers',
                   'number_of_edits',
                   'followers',
                   'following'])

def get_user_id(u):
    return urlparse.urljoin('http://www.quora.com/', u['link'])

def is_anonymous(u):
    return u['link'] is None or u['name'] is 'Anonymous'

def get_all_users():
    files = glob.glob('users/*.json')
    users = []
    for f in files:
        with codecs.open(f, encoding='utf-8') as f:
            data = json.load(f)
            user = create_user_from(data)
            users.append(user)
    return users

def create_user_from(data):
    user_id   = data['url']
    topics    = data['topics']
    blogs     = data['blogs']
    questions = data['questions']
    answers   = data['answers']
    edits     = data['edits']
    followers = map(get_user_id, data['followers'])
    following = map(get_user_id, data['following'])
    return User(user_id, topics, blogs, questions, answers, edits,
                followers, following)

def user_to_string(user):
    return '%s, %s, %s, %s, %s, %s, {{{%s}}}, {{{%s}}}' % \
           (user.user_id, user.number_of_topics, user.number_of_blogs,
            user.number_of_questions, user.number_of_answers,
            user.number_of_edits, ', '.join(user.followers),
            ', '.join(user.following))

def write_users(users, csv_filename):
    with open(csv_filename, 'w') as f:
        for user in users:
            f.write(user_to_string(user))
            f.write('\n')

def main():
    users = get_all_users()
    write_users(users, 'out/users.csv')

if __name__ == '__main__':
    main()