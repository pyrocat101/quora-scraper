<TeXmacs|1.0.7.20>

<style|generic>

<\body>
  <doc-data|<doc-title|EE239AS Project 1>|<doc-author|<\author-data|<author-name|Linjie
  Ding, Qirong Wang, Qinkai >>
    \;
  </author-data>>>

  <section|Task>

  We crawled answers under the Quora topic <with|font-shape|italic|Smoking
  Bans,> <with|font-shape|italic|Mental Health Hacks,>
  <with|font-shape|italic|Cigarettes> and <with|font-shape|italic|Bongs,> and
  information of users appeared in those answers (including non-anonymous
  authors and answers).

  <section|Approach>

  Instead of using <with|font-shape|italic|Selenium><\footnote>
    <hlink|http://docs.seleniumhq.org/|>
  </footnote>, which is slow and unscalable, we used
  <with|font-shape|italic|Casperjs><\footnote>
    <hlink|http://casperjs.org/|>
  </footnote>, which is a scriptable Webkit browser built on top of
  <with|font-shape|italic|PhantomJS.> Our approach has several advantages
  over Selenium:

  <\enumerate-numeric>
    <item>It is scalable. Since CasperJS is a command-line tool does not
    require desktop environment, we can run our crawler on server-side. In
    our case, we used SEASnet Linux server and CSD Linux server to crawl our
    data.

    <item>It is fast. Since CasperJS has no GUI, media resources like images
    are not loaded. Moreover, we don't have the overhead of rendering, which
    would be a significant performance hit when there are many elements in
    the same page.

    <item>It can be scripted using JavaScript. CasperJS is scripted using
    JavaScript, which allows us to seamlessly interact with elements in the
    page, like sending mouse event and precisely select element using CSS
    selectors.

    <item>It is parallable. We can run multiple instances of CasperJS to
    crawl pages in parallel, which is much faster than single-instance
    crawler.
  </enumerate-numeric>

  <section|Performance>

  Our crawler is built for speed from the start:

  <\enumerate-numeric>
    <item>The scrolling interval is adaptive with the loading speed of the
    content;

    <item>The voters are expanded on the fly as the answers are loaded;

    <item>The content is scraped directly from DOM without saving HTML and
    parse it;

    <item>A multi-processing script is used to spawn multiple worker
    crawlers;
  </enumerate-numeric>

  In the evaluation, it took 1,032.64 seconds to crawl all the answers of the
  given topic, using 4 worker crawlers in parallel. And it took six hours to
  crawl all the user information, using SEASnet Linux servers and CSD Linux
  servers, with 10 worker process and 7 worker process respectively. We
  collected 520 questions, 1,629 answers and 5,881 users in total.
</body>

<\initial>
  <\collection>
    <associate|font-base-size|12>
    <associate|page-type|letter>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-2|<tuple|2|1>>
    <associate|auto-3|<tuple|3|1>>
    <associate|auto-4|<tuple|3|?|../../../../.TeXmacs/texts/scratch/no_name_2.tm>>
    <associate|footnote-1|<tuple|1|1>>
    <associate|footnote-2|<tuple|2|1>>
    <associate|footnr-1|<tuple|1|1>>
    <associate|footnr-2|<tuple|2|1>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Task>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Approach>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Performance>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>